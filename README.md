# README #

## About the project ##
The project retrieves data for 5 European cities from OpenWeatherMaps

Click on City name, you get 5 days forecast.

## How to run ##
   Clone the project from github ```https://lvaoliveira@bitbucket.org/lvaoliveira/weather.git```
   
   Install bower globally if you don't have it
   
   Run ```bower install``` in terminal
   
   Open index.html from project folder

## Why

1. **No bundle:**

    To achieve the requirement of run the exercise by opening index.html, I choose to not use any bundle system.
    
2. **No templates:**

    Since I don't have any bundle system, I may not use templates, because if I try to inject the HTML with ng-template, I will get a CORS error. To prevent it I should run a webserver instead of opening the index.html.
    
3. **Weather Icons:**
 
    I choose Weather Icons to represent the weather condition with icons

4. **Bootstrap and Css:**

    I choose Bootstrap because it's well tested and used, it's increase the code time. I also used some personal styling to fit my design.
    
5. **Code Standarts:**

    I tried to follow John Papa code standarts, to make it more readble and scalable.

6. **Hightcharts:**

    I used Hightcharts as chart libary because I have some previous experience with it.

## UI dependecies ##
```
angular 1.5
angular ui router 0.3.1
weather icons 2.0.10
bootstrap 3.3.7
highcharts 5.0.9
```