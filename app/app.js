'use strict';

angular.module('BackBaseApp', [
  'ui.router'
])
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('default', {
      url: '/',
      templateUrl: 'partials/main.html',
      controller: 'mainController',
      controllerAs: 'vm'
    })
    .state('city', {
      url: '/city/:id',
      templateUrl: 'partials/city.html',
      controller: 'cityController',
      controllerAs: 'vm'
    })

  $urlRouterProvider.otherwise(function($injector) {
    var $state = $injector.get('$state');

    $state.go('default');
  });
});