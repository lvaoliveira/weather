'use strict';
angular
  .module('BackBaseApp')
  .controller('mainController', Main);

Main.$inject = ['$api', '$q'];

function Main($api, $q) {

  function init(self){
    var vm = self;
    var aResponse = [];
    var aPromisses = [];
    var cities = [
      'London',
      'Amsterdam',
      'Lisbon',
      'Berlin',
      'Paris'
    ];
    for(var i = 0; i <cities.length; i++) {
      aPromisses.push($api.getWeatherByCity(cities[i]).then(function(response){
        aResponse.push(response);
      }));
    }

    $q.all(aPromisses).then(function(){
      vm.cities = aResponse;
    });

    vm.tempFormat = function(temp) {
      return Math.floor(temp);
    }
  }

  init(this);
}

