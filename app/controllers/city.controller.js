'use strict';
angular
  .module('BackBaseApp')
  .controller('cityController', Main);

Main.$inject = ['$api', '$q', '$state'];

function Main($api, $q, $state) {

  function plotChart(aDate, aForecast) {
    Highcharts.chart('chart-container', {
      chart: {
        type: 'areaspline'
      },
      xAxis: {
        categories: aDate
      },
      credits: {
        enabled: false
      },

      title: {
        text: null
      },
      yAxis: {
        title: {
          text: ''
        }
      },
      series: [{
        showInLegend: false,
        color: '#FFE168',
        name: 'Forecast at 12:00',
        data: aForecast
      }]
    });
  }

  function init(self) {
    var vm = self;
    if($state.params.id) {
      $api.getForecastByCityId($state.params.id).then(function (response) {
        vm.city = response.city;
        vm.list = [];
        var aForecast = [];
        var aDate = [];
        var aList = response.list;
        for(var i =0; i < aList.length; i++ ){
          var date = aList[i].dt_txt.split(' ');
          if(date[1] === '12:00:00'){
            var day = date[0].split('-');
            var date = day[2]+'/'+day[1]+'/'+day[0];
            vm.list.push({
              'date': date,
              'weather': aList[i].weather[0],
              'main': aList[i].main
            });
            aForecast.push(aList[i].main.temp);
            aDate.push(date);
          }
        }
        plotChart(aDate, aForecast);
      });
    }
  }
  init(this);
}

