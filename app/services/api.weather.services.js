'use strict';

angular.module('BackBaseApp')
.service('$api', [
  '$http',
  '$state',
  '$q',
  function($http, $state, $q) {
    var baseUrl = 'http://api.openweathermap.org/data/2.5/';
    var appId = 'dbbfe8b9e9bd0882b6bf2164add13ef0';
    var units = 'metric'
    this.getWeatherByCity = function(city) {
      var url = baseUrl + 'weather?q=' + city + '&appid=' + appId + '&units=' + units;
      var deferred = $q.defer();
      $http.get(url, {
        cache: false,
      })
      .then(function(response) {
        if (response && response.data) {
            deferred.resolve(response.data);
        } else {
          deferred.reject(response);
        }
      }, function(response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };

    this.getForecastByCityId = function(cityId) {
      var url = baseUrl + 'forecast?id=' + cityId  + '&appid=' + appId + '&mode=json' + '&units=' + units,
          deferred = $q.defer();

      $http.get(url, {
        cache: false,
      })
      .then(function(response) {
        if (response && response.data) {
          deferred.resolve(response.data);
        } else {
          deferred.reject(response);
        }
      }, function(response) {
        deferred.reject(response);
      });

      return deferred.promise;
    };
  }
]);